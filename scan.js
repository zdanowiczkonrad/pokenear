const fetch = require('node-fetch');
const pokedex = require('./pokedex.json');

const serverPath = process.env.POKE_SERVER_PATH || 'http://localhost:5000';
 
const pad = number => number > 9 ? number : '0' + number;


const formatExpirationDate = expirationDate => {
	const secondsLeft = Math.round(expirationDate - (Date.now()/1000));
	return `${Math.floor(secondsLeft/60)} min ${pad(secondsLeft%60)} s`;
};

const getDistance = (lat1, lon1, lat2, lon2) => {
  const p = 0.017453292519943295;    // Math.PI / 180
  const c = Math.cos;
  const a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

const isWorthIt = pokemon => true;
const formatData = (pokemon, lat, lon) => {
	const id = pokemon.pokemonId;
	const name = pokedex[pokemon.pokemonId - 1].name;
	const when = formatExpirationDate(pokemon.expiration_time);
	const coords = pokemon.latitude + ',' + pokemon.longitude;
	const distance = Math.ceil(getDistance(lat, lon, pokemon.latitude, pokemon.longitude) * 1000);
	const directions = `https://www.google.com/maps/dir/${lat},${lon}/${coords}/@${lat},${lon},16z`;
	return { id, name, when, coords, distance, directions };
}

module.exports = (lat, lon) => {
	if(!lat || !lon) return false;
	return fetch(`${serverPath}/${lat}/${lon}`)
	.then(res => res.json())
	.then(pokemons => pokemons.pokemon
		.filter(isWorthIt)
		.map(poke => formatData(poke, lat, lon))
	).catch(err => {console.log(err)});
};

