# PokeNear
List all Pokemons that are nearby you and get the exact directions and how much time you have to catch it!

```bash
npm install
# required
export POKE_SERVER_PATH="host"
# your location as lat,lon
node index.js 51.1111316,17.0277717
```