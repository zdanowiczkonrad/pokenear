const scan = require('./scan');

const spacePad = (width, text) => {
	if (text.length < width) {
		return text + getLine(width - text.length);
	}
	else return text;
}

const printPoke = (data) => {	
	const { name, when, coords, distance, directions } = data;
	console.log(`${spacePad(18, name)} 🕒 ${spacePad(15, when)} 📏 ${spacePad(5, distance + ' m')}  🗺 ${directions}`);
};

const getLine = (length, character) => {
	let line = '';
	for (let i = 0; i < length; i++) {
		line += character || ' ';
	}
	return line;

}


let lat = process.env.USER_LATITUDE || 51.1112529;
let lon = process.env.USER_LONGITUDE || 17.0268489;

if (process.argv[2] && process.argv[2].indexOf(',') > -1) {
	console.log(`Using custom location: ${process.argv[2]}`);
	lat = +process.argv[2].split(',')[0];
	lon = +process.argv[2].split(',')[1];
} else {
	console.log(`Using default location for scanning: ${lat},${lon}`);
}
console.log(getLine(39, '-~'));

scan(lat, lon)
.then(pokes => pokes.map(printPoke))
.catch(console.log);